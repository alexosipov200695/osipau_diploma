BASE_URL = "https://restful-booker.herokuapp.com"

AUTH = BASE_URL + "/auth"

HEALTH = BASE_URL + "/ping"

BOOKINGIDS = BASE_URL + "/booking"

BOOKINGID = BASE_URL + "/booking/"


