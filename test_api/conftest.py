import pytest

from json import dumps, loads

import requests

from test_api import api_urls

from test_api.headers import HEADERS

import os

from datetime import datetime


@pytest.fixture()
def auth_data():
    data = {
        "username": "admin",
        "password": "password123"
    }
    return data

@pytest.fixture()
def booking_id():
    booking_ids_response = requests.get(
        url=api_urls.BOOKINGIDS,
        headers=HEADERS
    )
    assert booking_ids_response.status_code == 200
    all_booking_ids = loads(booking_ids_response.text)
    id_of_booking = all_booking_ids[0]
    booking_id = str(id_of_booking['bookingid'])
    return booking_id

@pytest.fixture()
def booking_data():
    data = {
    "firstname" : "Jim",
    "lastname" : "Brown",
    "totalprice" : 111,
    "depositpaid" : True,
    "bookingdates" : {
        "checkin" : "2018-01-01",
        "checkout" : "2019-01-01"
    },
    "additionalneeds" : "Breakfast"
}
    return data

@pytest.fixture()
def updated_booking_data():
    data = {
    "firstname": "James",
    "lastname": "Brown",
    "totalprice": 121,
    "depositpaid": True,
    "bookingdates": {
        "checkin": "2018-01-05",
        "checkout": "2019-02-01"
    },
    "additionalneeds": "Breakfast"
    }
    return data

@pytest.fixture()
def partial_update_data():
    data = {
    "bookingdates": {
        "checkin": "2018-03-25",
        "checkout": "2019-01-15"
        }
    }
    return data

@pytest.fixture()
def token(auth_data):
    auth_response = requests.post(
        url=api_urls.AUTH,
        headers=HEADERS,
        data=dumps(auth_data)
    )
    assert auth_response.status_code == 200
    token = loads(auth_response.text)['token']
    headers_with_token = {"Content-Type": "application/json",
                          "Cookie": f"token={token}"
    }
    return headers_with_token

@pytest.hookimpl(tryfirst=True)
def pytest_configure(config):
    if not os.path.exists('reports'):
        os.makedirs('reports')
    config.option.htmlpath = 'reports/'+datetime.now().strftime("%d-%m-%Y %H-%M-%S")+".html"