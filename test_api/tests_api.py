import requests

from json import loads, dumps

from test_api import api_urls

from test_api.headers import HEADERS

import allure

import os
import glob

def test_clear_allure_directory():
    files = glob.glob('/Users/mac/osipau_diploma/tmp/**/*.json', recursive=True)
    for f in files:
        try:
            os.remove(f)
        except OSError as e:
            print("Ошибка: %s : %s" % (f, e.strerror))

@allure.story('Test api diploma')
class TestBooking:

    @allure.story('Authorisation')
    def test_auth(self, auth_data):
        auth_response = requests.post(
            url=api_urls.AUTH,
            headers=HEADERS,
            data=dumps(auth_data)
    )
        assert auth_response.status_code == 200

    @allure.story('Health check')
    def test_health_check(self):
        health_response = requests.get(
            url=api_urls.HEALTH,
            headers=HEADERS
        )
        assert health_response.status_code == 201

    @allure.story('Create booking')
    def test_create_booking(self, booking_data):
        create_booking_response = requests.post(
            url=api_urls.BOOKINGIDS,
            headers=HEADERS,
            data=dumps(booking_data)
        )
        assert create_booking_response.status_code == 200
        all_booking_data = loads(create_booking_response.text)
        created_booking_id = str(all_booking_data["bookingid"])
        get_created_booking_response = requests.get(
            url=api_urls.BOOKINGID + created_booking_id,
            headers=HEADERS
        )
        assert get_created_booking_response.status_code == 200
        assert loads(get_created_booking_response.text)['lastname'] == booking_data['lastname']

    @allure.story('All bookings by ids')
    def test_bookings_ids(self):
        booking_ids_response = requests.get(
            url=api_urls.BOOKINGIDS,
            headers=HEADERS
        )
        assert booking_ids_response.status_code == 200

    @allure.story('Booking by id')
    def test_booking_id(self, booking_id):
        booking_id_response = requests.get(
            url=api_urls.BOOKINGID + booking_id,
            headers=HEADERS
        )
        assert booking_id_response.status_code == 200

    @allure.story('Update booking')
    def test_booking_update(self, token, updated_booking_data, booking_id):
        update_booking_response = requests.put(
            url=api_urls.BOOKINGID + booking_id,
            headers=token,
            data=dumps(updated_booking_data)
        )
        assert update_booking_response.status_code == 200
        get_update_booking_response = requests.get(
            url=api_urls.BOOKINGID + booking_id,
            headers=HEADERS
        )
        assert get_update_booking_response.status_code == 200
        assert loads(get_update_booking_response.text) == updated_booking_data

    @allure.story('Update (partially) booking')
    def test_partial_update(self, token, partial_update_data, booking_id):
        partial_update_response = requests.patch(
            url=api_urls.BOOKINGID + booking_id,
            headers=token,
            data=dumps(partial_update_data)
        )
        assert partial_update_response.status_code == 200
        get_partial_update_response = requests.get(
            url=api_urls.BOOKINGID + booking_id,
            headers=HEADERS
        )
        assert get_partial_update_response.status_code == 200
        assert loads(get_partial_update_response.text)['bookingdates'] == partial_update_data['bookingdates']

    @allure.story('Delete booking')
    def test_delete_booking(self, token, booking_id):
        delete_booking_response = requests.delete(
            url=api_urls.BOOKINGID + booking_id,
            headers=token
        )
        assert delete_booking_response.status_code == 201
        get_deleted_booking_response = requests.get(
            url=api_urls.BOOKINGID + booking_id,
            headers=HEADERS
        )
        assert get_deleted_booking_response.status_code == 404
