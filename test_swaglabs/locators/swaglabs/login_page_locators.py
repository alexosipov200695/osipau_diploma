from selenium.webdriver.common.by import By

LOGINPAGE_URL = "https://www.saucedemo.com/"
USERNAME_INPUT = (By.XPATH, '//input[@id="user-name"]')
PASSWORD_INPUT = (By.XPATH, '//input[@id="password"]')
LOGIN_BUTTON = (By.XPATH, '//input[@id="login-button"]')