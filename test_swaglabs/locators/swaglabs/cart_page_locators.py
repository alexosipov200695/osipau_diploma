from selenium.webdriver.common.by import By

CART_TITLE = (By.XPATH, '//span[@class="title"]')
ITEM_NAME = (By.XPATH, '//div[contains(text(), "Sauce Labs Bolt T-Shirt")]')
REMOVE_ITEM_BUTTON = (By.XPATH, '//button[@id="remove-sauce-labs-bolt-t-shirt"]')
ITEM_IN_CART = (By.XPATH, '//div[@class="inventory_item_name"]')
CHECKOUT_BUTTON = (By.XPATH, '//button[@class="btn btn_action btn_medium checkout_button"]')
FIRST_NAME_INPUT = (By.XPATH, '//input[@id="first-name"]')
LAST_NAME_INPUT = (By.XPATH, '//input[@id="last-name"]')
POSTAL_CODE_INPUT = (By.XPATH, '//input[@id="postal-code"]')
CONTINUE_BUTTON = (By.XPATH, '//input[@class="submit-button btn btn_primary cart_button btn_action"]')
FINISH_BUTTON = (By.XPATH, '//button[@class="btn btn_action btn_medium cart_button"]')
PAYMENT_SUCCESS_TEXT = (By.XPATH, '//h2[contains(text(), "THANK YOU FOR YOUR ORDER")]')