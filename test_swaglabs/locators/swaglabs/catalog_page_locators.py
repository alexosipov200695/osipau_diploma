from selenium.webdriver.common.by import By

MENU_BUTTON = (By.XPATH, '//button[@id="react-burger-menu-btn"]')
LOGOUT_BUTTON_IN_MENU = (By.XPATH, '//a[@id="logout_sidebar_link"]')

def product_name(name):
    return (By.XPATH, f'//div[@class="inventory_item_name"][contains(text(), "{name}")]')

ITEM_IN_CATALOG = (By.XPATH, '//div[@class="inventory_item"]')
SORT_MENU = (By.XPATH, '//select[@class="product_sort_container"]')
ZA_SORT = (By.XPATH, '//option[@value="za"]')
ADD_BACKPACK_TO_CART = (By.XPATH, '//button[@id="add-to-cart-sauce-labs-backpack"]')
BACKPACK_ITEM_NAME_IN_CATALOG = (By.XPATH, '//div[@class="inventory_item_name"][contains(text(), "Sauce Labs Backpack")]')
