from selenium.webdriver.common.by import By

ADD_TO_CART_BUTTON = (By.XPATH, '//button[@class="btn btn_primary btn_small btn_inventory"]')
REMOVE_FROM_CART_BUTTON = (By.XPATH, '//button[@class="btn btn_secondary btn_small btn_inventory"]')
CART_BUTTON = (By.XPATH, '//div[@id="shopping_cart_container"]')
PRODUCT_IN_PRODUCT_DETAILS = (By.XPATH, '//div[@class="inventory_details_name large_size"]')