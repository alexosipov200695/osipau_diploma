from test_swaglabs.pages.swaglabs.login_page import LoginPage
from test_swaglabs.pages.swaglabs.catalog_page import CatalogPage
from test_swaglabs.pages.swaglabs.product_page import ProductPage
from test_swaglabs.pages.swaglabs.cart_page import CartPage
import os
import glob
import allure

def test_clear_allure_directory_json():
    files = glob.glob('/Users/mac/osipau_diploma/tmp/**/*.json', recursive=True)
    for f in files:
        try:
            os.remove(f)
        except OSError as e:
            print("Ошибка: %s : %s" % (f, e.strerror))

def test_clear_allure_directory_txt():
    files = glob.glob('/Users/mac/osipau_diploma/tmp/**/*.txt', recursive=True)
    for f in files:
        try:
            os.remove(f)
        except OSError as e:
            print("Ошибка: %s : %s" % (f, e.strerror))

@allure.story('Login test')
def test_login(driver):
    login_page = LoginPage(driver)
    login_page.open_login_page()
    login_page.input_username()
    login_page.input_password()
    login_page.click_on_login_button()
    catalog_page = CatalogPage(driver)
    catalog_page.check_user_is_logged_in()

@allure.story('Logout test')
def test_logout(driver, login):
    catalog_page = CatalogPage(driver)
    catalog_page.click_on_menu_button()
    catalog_page.click_on_logout_button()
    login_page = LoginPage(driver)
    login_page.check_login_button()

@allure.story('Test of adding a product to the cart from product details page')
def test_add_product_to_cart_from_product_details(driver, login):
    catalog_page = CatalogPage(driver)
    catalog_page.open_product_details("Sauce Labs Bolt T-Shirt")
    product_page = ProductPage(driver)
    product_page.click_on_add_to_cart_button()
    cart_page = CartPage(driver)
    cart_page.check_item_presence_in_a_cart()

@allure.story('Test of deleting a product from the cart')
def test_remove_product_from_cart(driver, login):
    catalog_page = CatalogPage(driver)
    catalog_page.open_product_details("Sauce Labs Bolt T-Shirt")
    product_page = ProductPage(driver)
    product_page.click_on_add_to_cart_button()
    cart_page = CartPage(driver)
    cart_page.check_item_presence_in_a_cart()
    cart_page.remove_item_from_cart()

@allure.story('Sorting test')
def test_sort(driver, login):
    catalog_page = CatalogPage(driver)
    first_item = catalog_page.get_first_item()
    catalog_page.click_on_sort_menu()
    catalog_page.select_za_sort()
    last_item = catalog_page.get_last_item()
    assert first_item == last_item


@allure.story('Test of adding a product to the cart from catalog page')
def test_product_to_cart_from_catalog(driver, login):
    calalog_page = CatalogPage(driver)
    calalog_page.click_on_add_to_cart_button_from_catalog()
    calalog_page.check_item_is_in_cart()

@allure.story('Payment test')
def test_payment(driver, login):
    calalog_page = CatalogPage(driver)
    calalog_page.click_on_add_to_cart_button_from_catalog()
    calalog_page.check_item_is_in_cart()
    cart_page = CartPage(driver)
    cart_page.click_on_checkout()
    cart_page.input_first_name("Cristiano")
    cart_page.input_last_name("Ronaldo")
    cart_page.input_postal_code("000000")
    cart_page.click_on_continue_button()
    cart_page.click_on_finish_button()
    cart_page.check_for_payment_success_text()





