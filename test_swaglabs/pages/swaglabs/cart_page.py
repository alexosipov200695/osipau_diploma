from test_swaglabs.base_page import BasePage
from test_swaglabs.locators.swaglabs import cart_page_locators
from test_swaglabs.locators.swaglabs import product_page_locators
from selenium.common.exceptions import TimeoutException

cart_title = 'YOUR CART'
payment_title = 'CHECKOUT: YOUR INFORMATION'
checkout_overview_title = 'CHECKOUT: OVERVIEW'

class CartPage(BasePage):

    def check_item_presence_in_a_cart(self):
        element = self.find_element(product_page_locators.PRODUCT_IN_PRODUCT_DETAILS)
        product_name = element.text
        self.find_element(product_page_locators.CART_BUTTON).click()
        cart_title_in_cart = self.find_element(cart_page_locators.CART_TITLE)
        assert cart_title_in_cart.text == cart_title
        elements = self.find_elements(cart_page_locators.ITEM_IN_CART)
        first_element_name = elements[-1].text
        assert product_name == first_element_name

    def remove_item_from_cart(self):
        self.find_element(cart_page_locators.ITEM_NAME)
        self.find_element(cart_page_locators.REMOVE_ITEM_BUTTON).click()
        try:
            self.find_element(cart_page_locators.ITEM_NAME)
        except TimeoutException:
            print("Item is removed!")

    def click_on_checkout(self):
        self.find_element(cart_page_locators.CHECKOUT_BUTTON).click()
        cart_title_in_cart = self.find_element(cart_page_locators.CART_TITLE)
        assert cart_title_in_cart.text == payment_title

    def input_first_name(self, text):
        self.find_element(cart_page_locators.FIRST_NAME_INPUT).send_keys(text)

    def input_last_name(self, text):
        self.find_element(cart_page_locators.LAST_NAME_INPUT).send_keys(text)

    def input_postal_code(self, text):
        self.find_element(cart_page_locators.POSTAL_CODE_INPUT).send_keys(text)

    def click_on_continue_button(self):
        self.find_element(cart_page_locators.CONTINUE_BUTTON).click()
        cart_title_in_cart = self.find_element(cart_page_locators.CART_TITLE)
        assert cart_title_in_cart.text == checkout_overview_title

    def click_on_finish_button(self):
        self.find_element(cart_page_locators.FINISH_BUTTON).click()

    def check_for_payment_success_text(self):
        self.find_element(cart_page_locators.PAYMENT_SUCCESS_TEXT)