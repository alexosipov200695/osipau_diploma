from test_swaglabs.base_page import BasePage
from test_swaglabs.locators.swaglabs import product_page_locators
from test_swaglabs.locators.swaglabs import cart_page_locators

cart_title = 'YOUR CART'

class ProductPage(BasePage):

    def click_on_add_to_cart_button(self):
        element = self.find_element(product_page_locators.ADD_TO_CART_BUTTON)
        button_name = element.text
        element.click()
        element2 = self.find_element(product_page_locators.REMOVE_FROM_CART_BUTTON)
        remove_button_name = element2.text
        assert button_name != remove_button_name

    def go_to_cart(self):
        self.find_element(product_page_locators.CART_BUTTON).click()
        element = self.find_element(cart_page_locators.CART_TITLE)
        assert element.text == cart_title


