from test_swaglabs.base_page import BasePage
from test_swaglabs.locators.swaglabs import catalog_page_locators
from test_swaglabs.locators.swaglabs import product_page_locators
from test_swaglabs.locators.swaglabs import cart_page_locators

cart_title = 'YOUR CART'

class CatalogPage(BasePage):

    def check_user_is_logged_in(self):
        self.find_element(catalog_page_locators.MENU_BUTTON)

    def click_on_menu_button(self):
        self.find_element(catalog_page_locators.MENU_BUTTON).click()

    def click_on_logout_button(self):
        self.find_element(catalog_page_locators.LOGOUT_BUTTON_IN_MENU).click()

    def open_product_details(self, name):
        element = self.find_element(catalog_page_locators.product_name(name))
        name_in_catalog = element.text
        element.click()
        element2 = self.find_element(product_page_locators.PRODUCT_IN_PRODUCT_DETAILS)
        name_in_details = element2.text
        assert name_in_catalog == name_in_details

    def get_first_item(self):
        locator = catalog_page_locators.ITEM_IN_CATALOG
        elements = self.find_elements(locator)
        first_item = elements[0]
        return first_item.text

    def click_on_sort_menu(self):
        self.find_element(catalog_page_locators.SORT_MENU).click()

    def select_za_sort(self):
        self.find_element(catalog_page_locators.ZA_SORT).click()

    def get_last_item(self):
        locator = catalog_page_locators.ITEM_IN_CATALOG
        elements = self.find_elements(locator)
        last_item = elements[-1]
        return last_item.text

    def click_on_add_to_cart_button_from_catalog(self):
        self.find_element(catalog_page_locators.ADD_BACKPACK_TO_CART).click()

    def check_item_is_in_cart(self):
        self.find_element(catalog_page_locators.BACKPACK_ITEM_NAME_IN_CATALOG)
        self.find_element(product_page_locators.CART_BUTTON).click()
        element = self.find_element(cart_page_locators.CART_TITLE)
        assert element.text == cart_title
        self.find_element(catalog_page_locators.BACKPACK_ITEM_NAME_IN_CATALOG)









