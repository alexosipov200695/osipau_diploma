from test_swaglabs.base_page import BasePage
from test_swaglabs.locators.swaglabs import login_page_locators

username = 'standard_user'
password = 'secret_sauce'

class LoginPage(BasePage):

    def open_login_page(self):
        self.driver.get(login_page_locators.LOGINPAGE_URL)

    def input_username(self):
        self.find_element(login_page_locators.USERNAME_INPUT).send_keys(username)

    def input_password(self):
        self.find_element(login_page_locators.PASSWORD_INPUT).send_keys(password)

    def click_on_login_button(self):
        self.find_element(login_page_locators.LOGIN_BUTTON).click()

    def check_login_button(self):
        self.find_element(login_page_locators.LOGIN_BUTTON)
