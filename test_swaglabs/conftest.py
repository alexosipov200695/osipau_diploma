import pytest
from selenium import webdriver
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.chrome.options import Options as ChromeOptions

from test_swaglabs.pages.swaglabs.login_page import LoginPage
from test_swaglabs.pages.swaglabs.catalog_page import CatalogPage


def pytest_addoption(parser):
    parser.addoption(
        "--browser",
        action="store",
        default="chrome",
        help="browser can be: chrome or firefox",
    )

@pytest.fixture(scope="function")
def driver(request):
    browser = request.config.getoption("--browser")

    geckodriver_path = "/Users/mac/osipau_diploma/geckodriver"
    chromedriver_path = "/Users/mac/osipau_diploma/chromedriver"

    download_path = "/Users/mac/selenium/Downloads"

    f_type = (
        "application/pdf"
        "vnd.ms-excel,"
        "application/vnd.ms-excel.addin.macroenabled.12,"
        "application/vnd.ms-excel.template.macroenabled.12,"
        "application/vnd.ms-excel.template.macapplication/vnd.ms-excel.sheet.binaryroenabled.12,"
        "application/vnd.ms-excel.sheet.macroenabled.12,"
        "application/octet-stream"
    )

    if browser == "firefox":
        options = FirefoxOptions()
        profile = webdriver.FirefoxProfile()
        profile.set_preference("browser.download.folderList", 2)
        profile.set_preference("browser.download.manager.showWhenStarting", False)
        profile.set_preference("browser.helperApps.alwaysAsk.force", False)
        profile.set_preference("browser.download.useDownloadDir", True)
        profile.set_preference("browser.download.dir", download_path)
        profile.set_preference("pdfjs.disabled", True)
        profile.set_preference("browser.helperApps.neverAsk.saveToDisk", f_type)
        driver = webdriver.Firefox(
            profile, executable_path=geckodriver_path, options=options
        )
        driver.maximize_window()

        yield driver
        driver.quit()

    elif browser == "chrome":
        chrome_options = ChromeOptions()
        prefs = {"download.default_directory": download_path}
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-setuid-sandbox")
        chrome_options.add_experimental_option("prefs", prefs)
        driver = webdriver.Chrome(
            executable_path=chromedriver_path, options=chrome_options
        )
        driver.maximize_window()

        yield driver
        driver.quit()

    elif browser == "selenoid_firefox":
        capabilities = {
            "browserName": "firefox",
            "browserVersion": "88.0",
            "selenoid:options": {
                "enableVNC": True,
                "enableVideo": False
            }
        }
        options = FirefoxOptions()
        options.set_preference("browser.download.folderList", 2)
        options.set_preference("browser.download.manager.showWhenStarting", False)
        options.set_preference("browser.helperApps.alwaysAsk.force", False)
        options.set_preference("browser.download.useDownloadDir", True)
        options.set_preference("browser.download.dir", download_path)
        options.set_preference("pdfjs.disabled", True)
        options.set_preference("browser.helperApps.neverAsk.saveToDisk", f_type)

        driver = webdriver.Remote(
            command_executor="http://localhost:4444/wd/hub",
            desired_capabilities=capabilities,
            options=options)

        driver.maximize_window()

        yield driver
        driver.quit()



@pytest.fixture(scope="function")
def login(driver):
    login_page = LoginPage(driver)
    login_page.open_login_page()
    login_page.input_username()
    login_page.input_password()
    login_page.click_on_login_button()
    catalog_page = CatalogPage(driver)
    catalog_page.check_user_is_logged_in()
